class TypeUser
{
    const IOS = 1;
    const ANDROID2 = 2;
}

class AppError
{
    const ERROR0 = '0';
    const ERROR1 = '1';
    const ERROR2 = '2';
    const ERROR3 = '3';
    const ERROR4 = '4';
    const ERROR5 = '5';
    const ERROR6 = '6';
}

class AppStatus
{
    const STATU1 = '1';
    const STATU2 = '2';
    const STATU3 = '3';
    const STATU4 = '4';
    const STATU5 = '5';
    const STATU6 = '6';
    
}
public funtion postConfirm()
{
    $serviceId = Input::get('service_id');
    $driverId = Input::get('driver_id');
    $servicio = Service::find($serviceId);

    if($servicio != NULL)
    {
        if ($servicio->status_id == AppStatus::STATUS6)
        {
            return Response::json(array('error'=>AppError::ERROR2));
        }
        if ($servicio->driver_id == NULL && $servicio->status_id == AppStatus::STATUS1)
        {
            $servicio = Service::update($serviceId, array(
                'driver_id' => $driverId,
                'status_id' => AppStatus::STATUS2
            ));
            Driver::update($driverId, array(
                "available"=>'0'
            ));
            $driverTmp = Driver::find($driverId);
            Service::update($serviceId, array(
                'car_id' => $driverTmp->car_id
            ));
            //Notificar a usuario!!
            $pushMessage =  'Tu servicio ha sido confirmado!';
            $servicio = Service::find($serviceId);
            $push = Push::make();
            if($service->user->uuid == '')
            {
              return Response::json(array('error'=>AppError::ERROR0));
            }
            if($service->user->type == TypeUser::IOS) {
                $result = $push->ios($service->user->uuid, $psuhMessage, 1, 'honk.wav', 'Open', array('serviceId' => $servicio->id));
            } else
            {
              $result = $push->android2($service->user->uuid, $psuhMessage, 1, 'default', 'Open', array('serviceId' => $servicio->id));
            }
            return Response::json(array('error'=>AppError::ERROR0));
        } else
        { 
          return Response::json(array('error'=>AppError::ERROR1));
        }
    } 
    return Response::json(array('error'=>AppError::ERROR3));
}

