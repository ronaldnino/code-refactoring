# Léeme #

Este repositorio guarda dos archivos que pretenden resolver la práctica 2 de la prueba ténica de BackEdn Rappi, estos son: código original y codigo refactorizado.

### Malas prácticas de programación detectadas en el código. ###

* Hard Code o valores harcodeado.
* Código obsoleto comentado.
* Mal uso de las variables globales.
* No sigue del todo estadares de programación.


### Soluciones que se pueden aplicar en la refactorización del código. ###

* La mala práctica de Hard Code se puede solucionar con la definición de constantes.
* Eliminar todo el código obsoleto que esta comentado.
* Eliminar el uso indiscriminado del Input::get para restar complejidad a la lectura del código y ejecución del código.
* Implementar estandares PSR-1 y PSR-4 para mejorar el estido del código
